FROM node:20.11.1 AS build

WORKDIR /app

COPY package.json ./
COPY yarn.lock ./

RUN yarn install --no-progress

COPY ./ .

ENV APP_DOCKERMODE 1

RUN sed -i 's/\r$//' /app/entrypoint.sh

RUN yarn build

FROM nginxinc/nginx-unprivileged:mainline-alpine as final

COPY --from=build /app/dist /app
COPY --from=build /app/entrypoint.sh /app
COPY --from=build /app/nginx.conf /etc/nginx/conf.d/default.conf

ARG UID=101
ARG GID=101

USER root

RUN chown -R $UID:$GID /app && chmod 700 /app

USER $UID

EXPOSE 8080

ENTRYPOINT ["sh", "/app/entrypoint.sh"]
