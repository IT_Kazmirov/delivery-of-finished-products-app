declare module "@it-enterprise/jwtauthentication" {
  export interface userData {
    exp?: number;
    family_name?: string;
    iat?: number;
    id: string;
    "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/authentication"?: string;
    login: string;
    nbf?: number;
    userName: string;
    userPhoto: string;
  }
  export default auth as {
    config: (config: {
      baseUrl: string;
      loginUrl: string;
      refreshUrl: string;
      loginByPINCodeUrl: string;
      loginByBarCodeUrl: string;
      closeSessionUrl: string;
      skipCheckLicense?: boolean;
      onError?: (error: unknown) => void;
    }) => void;
    login: (
      login: string,
      password: string,
      rememberMe?: boolean
    ) => Promise<{
      success: boolean;
      errorMessage: string;
    }>;
    loginByPINCode: (code: string) => Promise<{
      success: boolean;
      errorMessage: string;
    }>;
    loginByBarCode: (code: string) => Promise<{
      success: boolean;
      errorMessage: string;
    }>;
    getToken: () => Promise<string | undefined>;
    clearTokens: () => Promise<void>;
    getUserData: () => Promise<userData>;
  };
}
