export type TranslateLocale = (key: string, number: number) => string;
