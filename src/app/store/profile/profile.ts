import { defineStore } from "pinia";
import { getDIContainer } from "@/shared/di";
import { UserDetails } from "@/shared/services";

interface State {
  userDetails: UserDetails | null;
}

export const useProfileStore = defineStore("profiles", {
  state: (): State => ({
    userDetails: null,
  }),
  actions: {
    async fetch() {
      if (this.userDetails) {
        return;
      }

      const { authService } = getDIContainer();
      const hasAuth = await authService.hasAuth();
      if (!hasAuth) {
        this.userDetails = null;
        return;
      }
      this.userDetails = (await authService.getUser()) || null;
    },
    async logOut() {
      this.userDetails = null;

      const { authService } = getDIContainer();
      await authService.logout();
    },
  },
});
