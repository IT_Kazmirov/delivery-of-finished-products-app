import { defineStore } from "pinia";
import { useToast } from "vue-toast-notification";
import "vue-toast-notification/dist/theme-sugar.css";

export const useNotifications = defineStore("notifications", () => {
  const toast = useToast({ position: "top-right", duration: 3 * 1000 });
  return {
    success(message: string): void {
      toast.success(message);
    },
    error(message: string): void {
      toast.error(message, { duration: 25 * 1000 });
    },
    warning(message: string): void {
      toast.warning(message);
    },
    info(message: string): void {
      toast.info(message);
    },
  };
});
