import { defineStore } from "pinia";
import { computed, ref } from "vue";
import {
  DeliveryInfo,
  Production,
  SetDeliveryQuantityRequest,
  SetProductQuantity,
  SetQuantityToCountry,
  useDeliveryDataApi,
} from "@/shared/domains";
import { useNotifications } from "..";

function groupProducts(deliveries: DeliveryInfo[]): Map<number, Production[]> {
  const map = new Map<number, Production[]>();
  deliveries.forEach((deliveryInfo) => {
    const key = deliveryInfo.DepartmentId;
    const collection = map.get(key);
    if (!collection) {
      map.set(key, deliveryInfo.Productions);
    }
  });

  return map;
}

export const useDeliveryDataSource = defineStore("delivery-data", () => {
  const deliveries = ref<DeliveryInfo[]>([]);
  const loading = ref(false);
  const deliveriesByDepartment = ref<Map<number, Production[]>>();
  const quantityLoading = ref(false);

  /**
   * Кількості були змінені
   */
  const isChangeQuantity = computed(() => {
    return deliveries.value.some((delivery) =>
      delivery.Productions.some((product) => product.QuantityToCountries.some((country) => country.InputQuantity > 0))
    );
  });

  async function fetch() {
    try {
      loading.value = true;

      const result = await useDeliveryDataApi().get();
      if (!result?.Success) {
        const notifications = useNotifications();
        notifications.error(result?.ErrorMessage || "Помилка при завантаженні даних");
      }
      deliveries.value = result?.Deliveries || [];
      deliveriesByDepartment.value = groupProducts(deliveries.value);
    } finally {
      loading.value = false;
    }
  }
  /**
   * Отримати продукцію підрозділу
   * @param departmentId ідентифікатор підрозділу
   * @returns продукція підрозділу
   */
  function getProducts(departmentId: number): Production[] {
    const collection = deliveriesByDepartment.value?.get(departmentId);
    return collection || [];
  }

  /**
   * Передати введені кількості
   * @param delivery продукція підрозділу
   */
  async function setQuantities(delivery: DeliveryInfo): Promise<boolean> {
    const notifications = useNotifications();
    let success = true;
    const infoMessage = delivery.DepartmentName.concat(". ");
    try {
      const deliveryQuantityRequest = new SetDeliveryQuantityRequest();
      deliveryQuantityRequest.DepartmentId = delivery.DepartmentId;

      delivery.Productions.forEach((production) => {
        const productQuantities = new SetProductQuantity();
        productQuantities.ResourceCode = production.ResourceCode;

        production.QuantityToCountries.forEach((country) => {
          if (country.InputQuantity > 0) {
            const countryQuantities = new SetQuantityToCountry(country.CountryCode, country.InputQuantity);
            productQuantities.QuantityToCountries.push(countryQuantities);
          }
        });

        deliveryQuantityRequest.Products.push(productQuantities);
      });

      let errorMessage = "";
      quantityLoading.value = true;

      if (
        !deliveryQuantityRequest.Products.some((product) =>
          product.QuantityToCountries.some((country) => country.InputQuantity > 0)
        )
      ) {
        return false;
      }

      const result = await useDeliveryDataApi().setQuantities(deliveryQuantityRequest);
      success = result?.Success || false;
      errorMessage = result?.ErrorMessage || "";

      if (success) {
        notifications.success(infoMessage.concat("Дані успішно оновлено"));
      } else {
        notifications.error(infoMessage.concat(errorMessage || "Помилка при оновленні даних"));
      }
      return success;
    } catch (error) {
      console.error(error);
      notifications.error(infoMessage.concat("Помилка при оновленні даних"));
    } finally {
      quantityLoading.value = false;
    }
    return success;
  }

  /**
   * Затвердити введені кількості по всім підрозділам
   */
  async function approveAllInputedQuantities() {
    let success = false;
    for (const delivery of deliveries.value) {
      if (await setQuantities(delivery)) {
        success = true;
        delivery.Productions.forEach((product) => product.QuantityToCountries.forEach((country) => (country.InputQuantity = 0)));
      }
    }
    if (success) {
      await fetch();
    }
  }

  /**
   * Оновити кількості в об'єкті
   * @param departmentId
   * @param resourceCode
   * @param countryCode
   * @param value
   * @returns
   */
  async function setQuantity(departmentId: number, resourceCode: string, countryCode: string, value: number) {
    const countryQuantities = deliveries.value
      .find((delivery) => delivery.DepartmentId === departmentId)
      ?.Productions.find((product) => product.ResourceCode === resourceCode)
      ?.QuantityToCountries.find((country) => country.CountryCode === countryCode);
    if (!countryQuantities) {
      console.log("Not found country for set quantity");
      return;
    }

    countryQuantities.InputQuantity = value;
    console.log("countryQuantities.InputQuantity = " + countryQuantities.InputQuantity);
  }

  return {
    deliveries,
    loading,
    quantityLoading,
    isChangeQuantity,
    fetch,
    getProducts,
    setQuantities,
    setQuantity,
    approveAllInputedQuantities,
  };
});
