import { Plugin } from "vue";
import { autoselectDirective } from "./autoselect";

export const globalDirectives: Plugin = {
  install(app) {
    app.directive("autoselect", autoselectDirective);
  },
};
