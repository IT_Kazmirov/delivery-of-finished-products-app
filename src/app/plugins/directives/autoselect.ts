export const autoselectDirective = {
  created(el: HTMLElement): void {
    const input = el.getElementsByTagName("input")[0] || el.getElementsByTagName("textarea")[0];
    if (input) {
      input.onfocus = () => {
        input.select();
      };
    }
  },
};
