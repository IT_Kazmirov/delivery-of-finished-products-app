import { httpClient } from "@/shared/transports";
import { AxiosError, AxiosResponse } from "axios";

const apiError = (error: AxiosError) => {
  if (error.response?.status !== 401) {
    const traceId = (error.response?.data as { traceId?: string })?.traceId;
    const message = "<p>Помилка при обробці запиту</p>" + (traceId ? `<p>Ідентифікатор запиту: ${traceId}</p>` : "");
    alert(message);
  }
  return Promise.reject(error);
};

httpClient.interceptors.response.use((response: AxiosResponse) => response, apiError);
