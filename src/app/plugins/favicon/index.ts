import { FAVICON_IMAGE } from "@/shared/constants";
import { Plugin } from "vue";

export const favicon: Plugin = {
  install() {
    if (FAVICON_IMAGE) {
      document.getElementById("favicon")?.setAttribute("href", FAVICON_IMAGE);
    }
  },
};
