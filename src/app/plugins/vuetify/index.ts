import "vuetify/styles";
import "@mdi/font/css/materialdesignicons.css";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
import * as directives from "vuetify/directives";
import * as labsComponents from "vuetify/labs/components";
import { mdi } from "vuetify/iconsets/mdi";

export const vuetify = createVuetify({
  components: {
    ...components,
    ...directives,
    ...labsComponents,
  },
  icons: {
    defaultSet: "mdi",
    sets: {
      mdi,
    },
  },
  theme: {
    defaultTheme: "light",
    themes: {
      light: {
        colors: {
          primary: "#ff7043",
          secondary: "#FFE0B2",
          accent: "#ff6e40",
          error: "#FF5252",
          info: "#1E88E5",
          success: "#1de9b6",
          "success-light": "#69f0ae",
          warning: "#FFC107",
          group: "#f5f5f5",
        },
      },
    },
  },
});
