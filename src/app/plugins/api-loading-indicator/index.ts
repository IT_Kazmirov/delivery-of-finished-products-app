import { httpClient } from "@/shared/transports";

httpClient.interceptors.request.use((config) => {
  // LoadingBar.start();
  return config;
});

httpClient.interceptors.response.use(
  (res) => {
    // LoadingBar.stop();
    return Promise.resolve(res);
  },
  (err) => {
    // LoadingBar.stop();
    return Promise.reject(err);
  }
);
