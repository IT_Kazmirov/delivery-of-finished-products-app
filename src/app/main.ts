import "@/app/assets/scss/global/index.scss";
import { getDIContainer, initDIContainer } from "@/shared/di";
import { createApp } from "vue";
import App from "@/app/App.vue";
import { router } from "@/app/router";
import { i18n } from "@/app/plugins/i18n";
import { favicon } from "@/app/plugins/favicon";
import "@/app/plugins/api-errors-handler";
import "@/app/plugins/api-loading-indicator";
import { API_URL, APPLICATION_VERSION, DI_KEY } from "@/shared/constants";
import { store } from "@/app/plugins/store";
import { vuetify } from "@/app/plugins/vuetify";
import { globalDirectives } from "./plugins/directives";

// Display version app
if (process.env.NODE_ENV === "production") {
  console.info(`%cVersion: ${APPLICATION_VERSION}`, "color:#5e5ce6; font-size:12px; font-weight: bold;");
}
initDIContainer();

getDIContainer().authService.refreshConfig({
  baseUrl: API_URL,
});

const app = createApp(App);
app.provide(DI_KEY, getDIContainer());
app.use(store);
app.use(vuetify);
app.use(i18n);
app.use(router);
app.use(favicon);
app.use(globalDirectives);
app.mount("#app");
