import { LanguageList } from "@/shared/constants";
import type { IntlNumberFormat, IntlNumberFormats } from "vue-i18n";

const defaultFormat: IntlNumberFormat = {
  currency: {
    style: "currency",
    currency: "UAH",
    currencyDisplay: "narrowSymbol",
  },
  quantity: {
    maximumFractionDigits: 3,
  },
  percent: {
    maximumFractionDigits: 1,
  },
};

export const numberFormats: IntlNumberFormats = {
  [LanguageList.Uk]: defaultFormat,
  [LanguageList.Ru]: defaultFormat,
  [LanguageList.En]: defaultFormat,
};
