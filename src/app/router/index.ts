import { getDIContainer } from "@/shared/di";
import { createRouter, createWebHistory } from "vue-router";
import { routes } from "./routes";
import { RouteName } from "@/shared/constants";

export const router = createRouter({
  history: createWebHistory(import.meta.env.VITE_APP_BASE_PATH),
  routes,
});

router.beforeEach(async (to, from, next) => {
  const hasAuth = await getDIContainer().authService.hasAuth();
  if (to.matched.some((record) => record.meta.needAuth)) {
    if (hasAuth) {
      next();
      return;
    }

    if (from.name !== RouteName.Login) {
      getDIContainer().reboundRepository.set(to.path);
      next({ name: RouteName.Login });
    } else {
      next(false);
    }
    return;
  }
  if (hasAuth && to.matched.some((record) => record.meta.needRedirect)) {
    next({ name: RouteName.Home });
    return;
  }
  next();
  return;
});
