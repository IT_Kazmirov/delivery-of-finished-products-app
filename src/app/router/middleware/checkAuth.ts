import { Middleware } from "@/app/router/types";
import { useProfileStore } from "@/app/store";

export const checkAuth: Middleware = async ({ di }) => {
  if (await di.authService.hasAuth()) {
    useProfileStore().fetch();
  }
};
