import { getDIContainer } from "@/shared/di";
import { h } from "vue";
import { RouteRecordRaw } from "vue-router";

export const signinRoute: RouteRecordRaw = {
  path: "/auth/signin",
  name: `signin`,
  component: {
    render: () => h("div"),
    created() {
      getDIContainer().authService.logout();
    },
  },
};
