import { getDIContainer } from "@/shared/di";
import { h } from "vue";
import { RouteRecordRaw } from "vue-router";

export const signoutRoute: RouteRecordRaw = {
  path: "/auth/signout",
  name: `signout`,
  component: {
    render: () => h("div"),
    created() {
      getDIContainer().authService.logout();
    },
  },
};
