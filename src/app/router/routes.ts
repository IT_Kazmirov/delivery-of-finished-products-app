import { RouteRecordRaw } from "vue-router";
import { RouteName, LayoutName } from "@/shared/constants";
import { loadLayout, loadPage } from "./utils";

export const routes: RouteRecordRaw[] = [
  {
    path: "/",
    component: loadLayout(LayoutName.Auth),
    redirect: { name: RouteName.Login },
    meta: {
      needAuth: false,
      needRedirect: true,
    },
    children: [
      {
        path: `/${RouteName.Login}`,
        name: RouteName.Login,
        component: loadPage(RouteName.Login),
      },
    ],
  },
  {
    path: "/",
    component: loadLayout(LayoutName.Default),
    redirect: { name: RouteName.Home },
    meta: {
      needAuth: true,
    },
    children: [
      {
        path: `/${RouteName.Home}`,
        name: RouteName.Home,
        component: loadPage(RouteName.Home),
      },
    ],
  },
  {
    path: `/${RouteName.AuthSignIn}`,
    name: RouteName.AuthSignIn,
    component: loadPage(RouteName.AuthSignIn),
  },
  {
    path: `/${RouteName.AuthSignInRedirect}`,
    name: RouteName.AuthSignInRedirect,
    component: loadPage(RouteName.AuthSignInRedirect),
  },
  {
    path: `/${RouteName.AuthSignOutRedirect}`,
    name: RouteName.AuthSignOutRedirect,
    component: loadPage(RouteName.AuthSignOutRedirect),
  },
  {
    path: `/${RouteName.AuthSignInSilentRedirect}`,
    name: RouteName.AuthSignInSilentRedirect,
    component: loadPage(RouteName.AuthSignInSilentRedirect),
  },
  { path: "/:pathMatch(.*)*", name: RouteName.NotFound, redirect: { name: RouteName.Home } },
];
