export const DEFAULT_FORMAT_DATE = "dd.MM.yyyy hh:mm";
export const DEFAULT_FORMAT_DATE_BRYNTUM = "DD.MM.YYYY HH:mm";
export const HOURS_DAY = 24;
