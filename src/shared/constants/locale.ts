export enum LanguageList {
  En = "En",
  Ru = "Ru",
  Uk = "Uk",
}

export const DEFAULT_LANG = LanguageList.Uk;
