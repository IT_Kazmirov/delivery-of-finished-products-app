export const DOCKERMODE = import.meta.env.VITE_APP_DOCKERMODE;
export const BASE_URL = import.meta.env.VITE_APP_BASE_PATH;
export const API_URL = import.meta.env.VITE_API_URL;
export const OIDC_AUTHORITY = import.meta.env.VITE_OIDC_AUTHORITY;
export const OIDC_CLIENTID = import.meta.env.VITE_OIDC_CLIENTID;
export const APPLICATION_VERSION = import.meta.env.VITE_APPLICATION_VERSION;
