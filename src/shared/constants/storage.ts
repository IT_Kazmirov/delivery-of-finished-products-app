export enum StorageKey {
  Lang = "lang",
  Rebound = "rebound_path",
  DarkMode = "app-darm-mode",
  FontSize = "app-font-size",
}
