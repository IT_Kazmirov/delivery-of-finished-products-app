export enum RouteName {
  Login = "login",
  Home = "home",
  NotFound = "not-found",
  AuthSignIn = "auth-signin",
  AuthSignInRedirect = "auth-signin-redirect",
  AuthSignOutRedirect = "auth-signout-redirect",
  AuthSignInSilentRedirect = "auth-signin-silent",
}

export enum LayoutName {
  Auth = "auth",
  Default = "default",
}
