export const headerCountryQuantity = [
  { title: "План", key: "QuantityPlan", width: "42px", sortable: false },
  { title: "Факт", key: "QuantityFact", width: "42px", sortable: false },
  { title: "Залишок", key: "QuantityStock", width: "42px", sortable: false },
  { title: "IN", key: "QuantityIn", width: "42px", sortable: false },
];

export const headerDelivery = [
  { title: "Продукція", key: "ResourceName", width: "150px", sortable: false },
  { title: "План", key: "QuantityPlan", width: "42px", sortable: false },
  { title: "Факт", key: "QuantityFact", width: "42px", sortable: false },
  { title: "Залишок", key: "QuantityStock", width: "42px", sortable: false },
];
