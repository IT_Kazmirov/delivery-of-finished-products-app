import auth from "@it-enterprise/jwtauthentication";

import { LoginType, type LoginFormData, type LoginResult, AuthPort, LoginData } from "../";

export function useAuth(): AuthPort {
  function loginResult(result?: LoginResult): LoginResult {
    if (!result?.success) {
      console.info("Unsuccessful login attempt: " + result?.errorMessage);
    }

    return { success: result?.success || false, errorMessage: result?.errorMessage || "Помилка при вході в систему" };
  }

  async function login({ login, password, rememberMe }: LoginData): Promise<LoginResult> {
    const result = await auth.login(login, password, rememberMe);

    return loginResult(result);
  }

  async function loginByPin(code: string): Promise<LoginResult> {
    const result = await auth.loginByPINCode(code);
    return loginResult(result);
  }

  async function loginByBarCode(code: string): Promise<LoginResult> {
    const result = await auth.loginByBarCode(code);
    return loginResult(result);
  }

  return {
    async loginByForm(data: LoginFormData): Promise<LoginResult> {
      let result = { success: false, errorMessage: "" };
      if (data.type === LoginType.Password && data.login) {
        result = await login({ login: data.login, password: data.password || "", rememberMe: data.rememberMe });
      }
      if (data.type === LoginType.Pin && data.code) {
        result = await loginByPin(data.code);
      }
      if (data.type === LoginType.Barcode && data.code) {
        result = await loginByBarCode(data.code);
      }
      return result;
    },
    async hasAuth() {
      const token = await auth.getToken();
      return Boolean(token);
    },
    async getToken() {
      try {
        const token = await auth.getToken();
        return token;
      } catch (error) {
        console.error("Error getting token: " + String(error));
        return undefined;
      }
    },
    async getUser() {
      try {
        const user = await auth.getUserData();
        return user;
      } catch (error) {
        console.error("Error getting user data: " + String(error));
        return undefined;
      }
    },
    async logout(): Promise<void> {
      await auth.clearTokens();
    },
    refreshConfig({ baseUrl, database }: { baseUrl: string; database?: string }): void {
      if (!baseUrl) {
        const error = "Auth refreshConfig: API base URL is not set";
        console.error(error);
        return;
      }

      const db = database ? `?db=${database}` : "";

      auth.config({
        baseUrl,
        loginUrl: `api/jwtlogin${db}`,
        loginByPINCodeUrl: `api/authentication/loginbypincode${db}`,
        loginByBarCodeUrl: `api/authentication/loginbybarcode${db}`,
        refreshUrl: `api/refreshtoken${db}`,
        closeSessionUrl: `api/web.closesession${db}`,
      });
    },
  };
}
