export interface LoginFormData {
  type: LoginType;
  login?: string;
  password?: string;
  code?: string;
  rememberMe?: boolean;
}

export enum LoginType {
  Password = "Password",
  Pin = "Pin",
  Barcode = "Barcode",
}

export interface LoginData {
  login: string;
  password: string;
  rememberMe?: boolean;
}

export interface LoginResult {
  success: boolean;
  errorMessage: string;
}

export interface UserDetails {
  id: string;
  login: string;
  userName: string;
  userPhoto: string;
}

export interface AuthPort {
  loginByForm: (data: LoginFormData) => Promise<LoginResult>;
  hasAuth: () => Promise<boolean>;
  getToken: () => Promise<string | undefined>;
  getUser: () => Promise<UserDetails | undefined>;
  logout: (path?: string) => Promise<void>;
  refreshConfig: ({ baseUrl, database }: { baseUrl: string; database?: string }) => void;
  // signinRedirectCallback(): Promise<void>;
  // signoutRedirectCallback(): Promise<void>;
}
