import axios, { AxiosError, AxiosResponse, InternalAxiosRequestConfig } from "axios";
import { API_URL } from "@/shared/constants";
import { getDIContainer } from "@/shared/di";
import { useAuth } from "../services";

export const httpClient = axios.create({
  baseURL: API_URL,
  headers: {
    "Content-Type": "application/json",
  },
  timeout: 80000,
});

const authToken = async (req: InternalAxiosRequestConfig<unknown>): Promise<InternalAxiosRequestConfig<unknown>> => {
  if (req?.headers) {
    const token = await getDIContainer().authService.getToken();
    if (token) {
      req.headers.Authorization = `Bearer ${token}`;
    }
  }
  return req;
};

const notValidToken = async (error: AxiosError) => {
  if (error.response?.status === 401) {
    const auth = useAuth();
    await auth.logout();
  }
  return Promise.reject(error);
};

httpClient.interceptors.request.use(authToken);
httpClient.interceptors.response.use((response: AxiosResponse) => response, notValidToken);
