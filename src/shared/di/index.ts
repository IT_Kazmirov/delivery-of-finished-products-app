import {
  AuthPort,
  LocalePort,
  LocaleRepositoryPort,
  ReboundRepositoryPort,
  useAuth,
  useLocale,
  useLocaleRepository,
  useReboundRepository,
} from "@/shared/services";

export interface DIContainer {
  localeRepository: LocaleRepositoryPort;
  localeService: LocalePort;
  reboundRepository: ReboundRepositoryPort;
  authService: AuthPort;
}

let diContainer: DIContainer;
initDIContainer();
export function initDIContainer(): void {
  diContainer = {
    localeRepository: useLocaleRepository(),
    localeService: useLocale({ localeRepository: useLocaleRepository() }),
    reboundRepository: useReboundRepository(),
    authService: useAuth(),
  };
}

export function getDIContainer(): DIContainer {
  return diContainer;
}
