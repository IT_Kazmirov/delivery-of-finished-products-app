export interface BaseRequestResult {
  Success: boolean;
  ErrorMessage: string;
}
