import { httpClient } from "@/shared/transports";
import { DeliveryDataApi } from "./port";
import { DeliveryDataResult, ISetDeliveryQuantityRequest, SetDeliveryQuantityResult } from "./types";

export function useDeliveryDataApi(): DeliveryDataApi {
  return {
    async get(): Promise<DeliveryDataResult | undefined> {
      const response = await httpClient.get("/api/_DELIVERYAPP.GETDELIVERYDATA");
      return response.data;
    },
    async setQuantities(request: ISetDeliveryQuantityRequest): Promise<SetDeliveryQuantityResult | undefined> {
      const response = await httpClient.post("/api/_DELIVERYAPP.SETDELIVERYDATA", request);
      return response.data;
    },
  };
}
