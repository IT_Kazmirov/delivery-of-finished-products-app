import { BaseRequestResult } from "@/shared/domains/base/type/base";

export interface DeliveryDataResult extends BaseRequestResult {
  Deliveries: DeliveryInfo[];
}

// Інформація про продукцію готову до передачі
export interface DeliveryInfo {
  DepartmentId: number;
  DepartmentName: string;
  Productions: Production[];
}

// Продукція
export interface Production {
  ResourceCode: string;
  ResourceName: string;
  Quntities: Quntities;
  QuantityToCountries: QuantityToCountry[];
}

// Кількості по країні
export interface QuantityToCountry extends SetQuantities {
  CountryCode: string;
  Quntities: Quntities;
}

// Кількості
export interface Quntities {
  Plan: number;
  Fact: number;
  Stock: number;
}

// Кількості для введення
export interface SetQuantities {
  InputQuantity: number;
}

export class SetDeliveryQuantityRequest implements ISetDeliveryQuantityRequest {
  public DepartmentId: number;
  public Products: SetProductQuantity[];
  public constructor() {
    this.DepartmentId = 0;
    this.Products = [];
  }
}

export interface ISetDeliveryQuantityRequest {
  DepartmentId: number;
  Products: SetProductQuantity[];
}

export class SetProductQuantity {
  public ResourceCode: string;
  public QuantityToCountries: SetQuantityToCountry[];
  public constructor() {
    this.ResourceCode = "";
    this.QuantityToCountries = [];
  }
}

export class SetQuantityToCountry {
  public CountryCode: string;
  public InputQuantity: number;
  public constructor(countryCode: string, quantity: number) {
    this.CountryCode = countryCode;
    this.InputQuantity = quantity;
  }
}

export type SetDeliveryQuantityResult = BaseRequestResult;
