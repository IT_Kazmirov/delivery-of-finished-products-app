import { DeliveryDataResult, ISetDeliveryQuantityRequest, SetDeliveryQuantityResult } from "./types";

export interface DeliveryDataApi {
  get: () => Promise<DeliveryDataResult | undefined>;
  setQuantities: (request: ISetDeliveryQuantityRequest) => Promise<SetDeliveryQuantityResult | undefined>;
}
