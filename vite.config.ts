import { fileURLToPath, URL } from "url";
import { defineConfig, loadEnv } from "vite";
import vue from "@vitejs/plugin-vue";
import checker from "vite-plugin-checker";
import vueI18n from "@intlify/vite-plugin-vue-i18n";

export default defineConfig(({ mode }) => {
  console.log(`mode: ${mode}`);

  const env = loadEnv(mode, process.cwd());
  console.log(`VITE_APP_BASE_PATH: ${env.VITE_APP_BASE_PATH}`);

  return {
    base: env.VITE_APP_BASE_PATH,
    plugins: [
      vue(),
      checker({
        vueTsc: true,
        overlay: {
          initialIsOpen: false,
        },
      }),
      vueI18n(),
    ],
    optimizeDeps: {
      include: [],
      exclude: [],
    },
    resolve: {
      alias: {
        "@": fileURLToPath(new URL("./src", import.meta.url)),
      },
    },
    server: {
      port: 3000,
    },
    build: {
      target: "esnext",
    },
  };
});
