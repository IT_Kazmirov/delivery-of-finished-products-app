#!/bin/sh
ROOT_DIR=/app

# Replace env vars in JavaScript files
echo "Replacing env constants in JS"
for file in $ROOT_DIR/*.js* $ROOT_DIR/assets/*.js* $ROOT_DIR/assets/*.css* $ROOT_DIR/*.html*
do
  echo "Processing $file ...";
  sed -i 's|\*APP_DOCKERMODE\*|'${APP_DOCKERMODE}'|g' $file
  sed -i 's|\*APP_BASE_PATH\*|'${APP_BASE_PATH}'|g' $file
  sed -i 's|\*APPLICATION_VERSION\*|'${APP_VERSION}'|g' $file
  sed -i 's|\*API_URL\*|'${API_URL_ENV}'|g' $file
  sed -i 's|\*OIDC_CLIENTID\*|'${OIDC_CLIENTID}'|g' $file
  sed -i 's|\*OIDC_AUTHORITY\*|'${OIDC_AUTHORITY}'|g' $file
  sed -i 's|\*DEFAULTLANGUAGE\*|'${DEFAULT_LANGUAGE_ENV}'|g' $file
  sed -i "s|\*FAVICON_IMAGE\*|$FAVICON_IMAGE|g" $file
done
echo "Starting Nginx"
nginx -g 'daemon off;'
